#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MaxLeng 100
#define MAxWords 2000

char nombrearchivo[37];
char nombrearchivosal[37];
char Plbra[MAxWords][MaxLeng];

typedef struct nodo {
                      char *NombreCter;
                      struct nodo *sgnte;
                    }Cter;

Cter *ListaC(Cter *Lista) {
                            Lista = NULL;
                            return Lista;
                           }

FILE *AbrirArchivo();
void CerrarArchivo(FILE *ptrFcro, char*nombrearchivo);
int CompArgumentos(int argc, char *argv[]);
int CrearArchivoE(char *NomArchivoCrear, int count);
Cter *addCharter(Cter *Lista, char *NombreCter);
void ShowListCharter(Cter *Lista);
void ShowWord(int count);
void BubbleSort(int count);
void CargarList(Cter *Lista, int count);
void CargarData(FILE *ptrArchDat, int count);


int main(int argc, char *argv[]) {

  int ArchivoExiste = 0, count;
  FILE *ptrFcro = NULL;
  Cter *Lista = ListaC(Lista);
  ArchivoExiste = CompArgumentos(argc, argv);
  if (ArchivoExiste == 0 ) {
                          ptrFcro = AbrirArchivo();
    if (ptrFcro != NULL) {
                            printf("El fichero -> %s  existe y se abrio exitosamente\n",nombrearchivo);
                            printf("\n\t\tL e y e n d o  F i c h e r o . . .\n");
                            system("sleep 2s");
                            count=0;

    while (feof(ptrFcro) == 0) {
                                 fscanf(ptrFcro, "%s", Plbra[count]);
                                 count++;
                                }

      printf("\n\n\t\tContenido desordenado\n");
      ShowWord(count);

      printf("\n\n\t\tO r d e n a n d o  P a l a b r a s . . .\n");
      system("sleep 2s");
      BubbleSort(count);

      printf("\n\n\t\tPalabras Ordenadas \n");
      ShowWord(count);

      printf("\n\n\t\tC a r g a n d o  L i s t a  E n l a z a d a  . . .\n");
      system("sleep 2s");
      CargarList(Lista, count);

      printf("\n\n\t\tC r e a n d o  F i c h e r o %s . . .\n\n",nombrearchivosal);
      CrearArchivoE(nombrearchivosal, count);
      CerrarArchivo(ptrFcro,nombrearchivo);
    }else{
      printf("\tEl  Fichero -> %s  No Existe\n", nombrearchivo);
    }
  }
  return 0;
}

void CerrarArchivo(FILE *ptrFcro, char *nombrearchivo){
                                                fclose(ptrFcro);
                                                printf("El fichero -> %s  Se ha cerrado exitosamente\n",nombrearchivo);
                                              }

Cter *addCharter(Cter *Lista, char *NombreCter){
                                                        Cter *NuevoCter, *aux;
                                                        NuevoCter=(Cter*)malloc(sizeof(Cter));
                                                        NuevoCter->NombreCter = NombreCter;
                                                        NuevoCter->sgnte = NULL;
                                                        if (Lista == NULL) {
                                                                            Lista = NuevoCter;
                                                                          }else{
                                                                                aux = Lista;
                                                                                while (aux-> sgnte!= NULL) {
                                                                                                          aux = aux->sgnte;
                                                                                                         }
                                                                                aux->sgnte = NuevoCter;
                                                                                }
                                                      return Lista;
                                                      }

void ShowListCharter(Cter *Lista){
                                    while (Lista != NULL) {
                                    printf("-------> %s \n",Lista->NombreCter);
                                    Lista=Lista->sgnte;
                                   }
                                   }

int CrearArchivoE(char *NomArchivoCrear, int count){
                                                        int StatusCreate = 0;
                                                        FILE *ptr_Fich_Create;
                                                        ptr_Fich_Create = fopen(NomArchivoCrear,"w");
                                                        if ( ptr_Fich_Create == NULL) {
                                                                                      printf("\n\nEl archivo -> %s  No fué creado\n",NomArchivoCrear);
                                                                                      }else{
                                                                                            printf("El archivo -> %s  Fue creado con exito\n",NomArchivoCrear);
                                                                                            StatusCreate = 1;
                                                                                            printf("Se cargarán datos en el fichero -> %s \n",NomArchivoCrear);
                                                                                            CargarData(ptr_Fich_Create, count);
                                                                                            CerrarArchivo(ptr_Fich_Create,nombrearchivosal);
                                                                                            }
                                                      return StatusCreate;
                                                      }

void BubbleSort(int count){
                            char PlbraAux[MaxLeng];
                            for(int i = 0; i < count-1; i++){
                            for(int j = 0; j < count-1; j++){
                            if(Plbra[j][0] > Plbra[j+1][0]){
                            strcpy(PlbraAux, Plbra[j]);
                            strcpy(Plbra[j],Plbra[j+1]);
                            strcpy(Plbra[j+1],PlbraAux);
                                                         }
                                                            }
                                                            }
                          }

void CargarData(FILE *ptrArchDat, int count){
                                                 for (int i = 0; i < count; i++) {
                                                                                  fputs(Plbra[i],ptrArchDat);
                                                                                  fputs("\n",ptrArchDat);
                                                                                }
                                                  }

FILE *AbrirArchivo(){
                  printf("El nombre del fichero a leer es -> %s \n",nombrearchivo );
                  FILE *archivo = fopen(nombrearchivo,"r");
                  return archivo;
                }
                
void ShowWord(int count){
                         for (int i = 0; i < count; i++) {
                         printf("-------> %s \n",Plbra[i]);
                                                          }
                        }

void CargarList(Cter *Lista, int count){
                                            for (int i = 0; i < count; i++) {
                                            Lista = addCharter(Lista, Plbra[i]);
                                          }
                                          printf("\n\t\tContenido de lista\n");
                                          ShowListCharter(Lista);
                                          } 

int CompArgumentos(int argc, char *argv[]){
  int error = 0;
  if (argc == 1) {
    printf("\nFaltan Argumentos De Entrada (nombrearchivo.txt) Y Salida (nombrearchivo.dat)\n Ejecute De Nuevo\n");
    error = 1;
  }else if(argc == 2){
    printf("\nFaltan Argumentos De Salida(nombrearchivo.dat)\n");
    error = 2;
  }else{
    printf("\n\t\tArgumentos Válidos.\n\n");
    strcpy(nombrearchivo,argv[1]);
    strcpy(nombrearchivosal,argv[2]);
    error = 0;
  }
  return error;
}