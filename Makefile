all:
	gcc -g -c -Wall Gestion.c -o Gestion.o
	gcc -g -Wall Gestion.o -o ControlFile

clean:
	rm *.o
	rm ControlFile
